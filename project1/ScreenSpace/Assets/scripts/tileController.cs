﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tileController : MonoBehaviour {


	GameObject myTile,tileParent,mybreadcrumb;

	Text tilePropertiesLabel;

	string currentLabel;

	bool boxGenerated = false;

	GameObject[] myTiles = new GameObject[4];


	tileMovementController movementScript;
	// Use this for initialization
	void Start () {
		myTile = Resources.Load<GameObject>("Tile");
		mybreadcrumb = Resources.Load<GameObject>("Breadcrumb");
		tileParent = GameObject.Find("TileParent");

		tilePropertiesLabel = 
		GameObject.Find("tilePropertiesText").GetComponent<Text>();

		//save the current text in the label
		currentLabel = tilePropertiesLabel.text;


		//create a tile in the middle of the screen, with no rotation
		/*
		for (int i = 0;i<4;i++){
			myTiles[i] = Instantiate(myTile,new Vector3(i,0f),Quaternion.identity);
			myTiles[i].GetComponent<SpriteRenderer>().color = Random.ColorHSV();
		}
		 */
		 StartCoroutine(generateBox());

		 tileParent.gameObject.AddComponent<tileMovementController>();

		

		movementScript = tileParent.gameObject.GetComponent<tileMovementController>();

		StartCoroutine(reverseMove());

	}

	IEnumerator reverseMove(){
		while (true){
		if (reverseMover){
			Debug.Log("test");
			movementScript.smoothorjerky = false;
			List<Vector3> positions = new List<Vector3>();
			if (movementScript.breadcrumbs != null){
				 positions = movementScript.breadcrumbs;
				positions.Reverse();
			
			}
			foreach(Vector3 pos in positions)
			{
				//take 1 second to move from one location to the other
				float time = 1f;
				//this is the amount of time which has passed so far 
				float elapsedTime = 0f;
				//save current position
				Vector3 startingPosition = tileParent.transform.position;
				while (elapsedTime<time){
					tileParent.transform.position = Vector3.Lerp(startingPosition,pos,elapsedTime/time);
					elapsedTime += Time.deltaTime;
					yield return null;
				}
				yield return null;
			}
			reverseMover = false;
			movementScript.smoothorjerky = true;
			yield return null;
		}else {
			yield return null;
		}

		}
	}

	IEnumerator generateBox()
	{
		for(int countery=0;countery<4;countery++)
		{
			for (int counterx=0;counterx<4;counterx++)
			{
				if ((countery==0)||(countery==3))
				{
					GameObject tile = Instantiate(myTile,new Vector3(counterx,countery),Quaternion.identity);
					tile.GetComponent<SpriteRenderer>().color = Random.ColorHSV();
					tile.name=counterx + "-"+countery;
					tile.transform.parent = tileParent.transform;
					yield return new WaitForSeconds(0.1f); 	
				}
				else if ((counterx==0) || (counterx==3)){
					GameObject tile = Instantiate(myTile,new Vector3(counterx,countery),Quaternion.identity);
					tile.GetComponent<SpriteRenderer>().color = Random.ColorHSV();
					tile.name=counterx + "-"+countery;
					tile.transform.parent = tileParent.transform;
					yield return new WaitForSeconds(0.1f); 	
				}
				
			}
			
		}
		boxGenerated = true;
		yield return null;

	}

	void showBreadCrumbs(){
		int counter = 0;
		foreach (GameObject g in GameObject.FindGameObjectsWithTag("breadcrumb")){
			Destroy(g);
		}
		foreach(Vector3 position in movementScript.breadcrumbs){
			counter++;
			GameObject dot = Instantiate(mybreadcrumb,position,Quaternion.identity);
			dot.GetComponentInChildren<TextMesh>().text = counter.ToString();
		}
	}

	void resetPath(){
		movementScript.breadcrumbs = new List<Vector3>();
		Debug.Log("cleared");
	}

	bool reverseMover = true;
	// Update is called once per frame
	void Update () {
		if (boxGenerated)
		{
			if (Input.GetKeyDown(KeyCode.Q)){
				movementScript.smoothorjerky = !movementScript.smoothorjerky;
			}
			if (Input.GetKeyDown(KeyCode.E)){
				showBreadCrumbs();
			}

			if (Input.GetKeyDown(KeyCode.R)){
				resetPath();
			}

			if (Input.GetKeyDown(KeyCode.M)){
				movementScript.smoothorjerky = false;
				reverseMover = !reverseMover;
			}


	
		}
		
	}
}
