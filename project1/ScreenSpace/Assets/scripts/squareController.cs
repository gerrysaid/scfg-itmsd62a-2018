﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class squareController : MonoBehaviour {

	//variables for the positions I am going to use
	Vector3[] positions = new Vector3[5];

	bool animate = false;
	bool animate2 = false;
	// Use this for initialization
	void Start () {
		//center point
		positions[0] = new Vector3(0f,0f);
		//top point
		positions[1] = new Vector3(0f,4.5f);
		//bottom point
		positions[2] = new Vector3(0f,-4.5f);

		//left point
		positions[3] = new Vector3(-5f*Camera.main.aspect,0f)+new Vector3(0.5f,0f);
		
		//right point
		positions[4] = new Vector3(5f*Camera.main.aspect,0f)-new Vector3(0.5f,0f);



		//at start, put the box in the middle of the screen
		this.transform.position = positions[0];

		StartCoroutine(animateSquare());
		StartCoroutine(animateSquareColor());
		StartCoroutine(task1());

	}

	bool tasks = false;

	IEnumerator task1()
	{
		float i = 0f;
		//reset the square to the middle
		transform.position = new Vector3(0f,0f);
		while (i<5f)
		{
			if (tasks)
			{
				transform.position = new Vector3(i*Camera.main.aspect,i);
				yield return new WaitForSeconds(1f);
				i++;
				
			}else{
				yield return null;
			}
		}
		yield return task2();
	}

	IEnumerator task2()
	{
		float i = 0f;
		//reset the square to the middle
		transform.position = new Vector3(0f,0f);
		while (i<5f)
		{
			if (tasks)
			{
				transform.position = new Vector3(-i*Camera.main.aspect,-i);
				yield return new WaitForSeconds(1f);
				i++;
				
			}else{
				yield return null;
			}
		}
		yield return task3();
	}

	
	IEnumerator task3()
	{
		float i = 0f;
		//reset the square to the middle
		transform.position = new Vector3(0f,0f);
		while (i<5f)
		{
			if (tasks)
			{
				transform.position = new Vector3(-i*Camera.main.aspect,i);
				yield return new WaitForSeconds(1f);
				i++;
				
			}else{
				yield return null;
			}
		}
		yield return task4();

	}

	IEnumerator task4()
	{
		float i = 0f;
		//reset the square to the middle
		transform.position = new Vector3(0f,0f);
		while (i<5f)
		{
			if (tasks)
			{
				transform.position = new Vector3(i*Camera.main.aspect,-i);
				yield return new WaitForSeconds(1f);
				i++;
				
			}else{
				yield return null;
			}
		}
		yield return null;

	}




	IEnumerator animateSquare()
	{
		//array index for the animation
		int i = 0;
		while(true){
			if (animate)
			{
				//do the animation
				this.transform.position = positions[i];
				yield return new WaitForSeconds(1f);
				i++;
				if (i>4)
				{
					i=0;
				}
			}else{
				//disappear and don't do anything
				yield return null;
			}
		}
	}


	IEnumerator animateSquareColor()
	{
		while(true)
		{
			if (animate2)
			{
				this.GetComponent<SpriteRenderer>().color = Color.red;
				yield return new WaitForSeconds(1f);
				this.GetComponent<SpriteRenderer>().color = Color.black;
				yield return new WaitForSeconds(1f);
			}
			else {
				yield return null;
			}
		}


	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.T))
		{
			tasks = !tasks;
		}

		if (Input.GetKeyDown(KeyCode.G))
		{
			animate = !animate;
		}

		if (Input.GetKeyDown(KeyCode.H))
		{
			animate2 = !animate2;
		}

		Debug.Log(animate);

		if (Input.GetKeyDown(KeyCode.W)){
			transform.position = positions[1];
		}

		if (Input.GetKeyDown(KeyCode.S)){
			transform.position = positions[2];
		}

		if (Input.GetKeyDown(KeyCode.A)){
			transform.position = positions[3]+new Vector3(0.5f,0f);
		}

		if (Input.GetKeyDown(KeyCode.D)){
			transform.position = positions[4]-new Vector3(0.5f,0f);
		}

		if (Input.GetKeyDown(KeyCode.Space)){
			transform.position = positions[0];
		}
		
	}
}
