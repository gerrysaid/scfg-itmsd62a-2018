﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tileMovementController : MonoBehaviour {

	public bool smoothorjerky = false;

	public float movementSpeed = 40f;
	// Use this for initialization

	public List<Vector3> breadcrumbs;
	
	void Start () {
		breadcrumbs = new List<Vector3>();
		StartCoroutine(smoothSampler());
	}

	IEnumerator smoothSampler(){
		breadcrumbs.Add(this.transform.position);
		while(true)
		{
			if (smoothorjerky)
			{
				if (breadcrumbs.Count>0)
				{
					//is last index equal to the current position?
					if (breadcrumbs[breadcrumbs.Count-1] != this.transform.position){
						breadcrumbs.Add(this.transform.position);
					}
				}
				yield return null;
			}
			else{
				//jerky movement is on, do nothing
				yield return null;
			}
		}
	}


	void smoothMove(){
		//horizontal smooth movement
		transform.Translate(Vector3.right * movementSpeed * Time.deltaTime * Input.GetAxis("Horizontal"));
		transform.Translate(Vector3.up * movementSpeed * Time.deltaTime * Input.GetAxis("Vertical"));
	}

	void jerkyMove(){
		if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				this.transform.position += new Vector3(0f,1f);
				breadcrumbs.Add(this.transform.position);
				
			}
			if (Input.GetKeyDown(KeyCode.DownArrow))
			{
				this.transform.position -= new Vector3(0f,1f);
				breadcrumbs.Add(this.transform.position);
				
			}
			if (Input.GetKeyDown(KeyCode.RightArrow))
			{
				this.transform.position += new Vector3(1f,0f);
				breadcrumbs.Add(this.transform.position);
			
			}
			if (Input.GetKeyDown(KeyCode.LeftArrow))
			{
				this.transform.position -= new Vector3(1f,0f);
				breadcrumbs.Add(this.transform.position);	
			}
			
	}
	
	
	// Update is called once per frame
	void Update () {
		if (!smoothorjerky){
			jerkyMove();
		}else {
			smoothMove();
		}
		
	}
}
