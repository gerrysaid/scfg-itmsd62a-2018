﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class thelettersScript : MonoBehaviour {


	GameObject tile;

	List<List<Vector3>> words;

	GameObject letterParent;
	// Use this for initialization

	IEnumerator drawWord(List<List<Vector3>> word){

		
	letterParent = new GameObject();
	
		foreach(List<Vector3> letters in word)
		{
			
			foreach(Vector3 p in letters)
			{
				GameObject myTile = Instantiate(tile,p,Quaternion.identity);
				myTile.transform.parent=letterParent.transform;
				yield return new WaitForSeconds(0.2f);
			}
			letterParent.transform.position -= new Vector3(4f,0f);
			yield return null;
		}
		letterParent.transform.localScale = new Vector3(0.5f,0.5f);
		letterParent.transform.position = new Vector3(-5f,0f);
		yield return null;
	}

	void Start () {
		List<List<Vector3>> words = new List<List<Vector3>>(); 
		tile = Resources.Load<GameObject>("LetterSquare");
		
		

		//GameObject lp = Instantiate(letterParent,new Vector3(0f,0f),Quaternion.identity);

		List<Vector3> letterG = new List<Vector3> {
			new Vector3(0f,0f),
			new Vector3(0f,1f),
			new Vector3(0f,2f),
			new Vector3(0f,3f),
			new Vector3(0f,4f),
			new Vector3(1f,4f),
			new Vector3(1.5f,2f),
			new Vector3(1f,0f),
			new Vector3(2f,4f),
			new Vector3(2f,2f),
			new Vector3(2f,1f),
			new Vector3(2f,0f)
		};

		
		List<Vector3> letterE = new List<Vector3> {
			new Vector3(0f,0f),
			new Vector3(0f,1f),
			new Vector3(0f,2f),
			new Vector3(0f,3f),
			new Vector3(0f,4f),
			new Vector3(1f,4f),
			new Vector3(1f,2f),
			new Vector3(1f,0f),
			new Vector3(2f,4f),
			new Vector3(2f,2f),
			new Vector3(2f,0f)
			
			
			
		};

		
		List<Vector3> letterR = new List<Vector3> {
			new Vector3(0f,0f),
			new Vector3(0f,1f),
			new Vector3(0f,2f),
			new Vector3(0f,3f),
			new Vector3(0f,4f),
			new Vector3(1f,4f),
			new Vector3(1f,2f),
			new Vector3(2f,0f),
			new Vector3(2f,2f),
			new Vector3(2f,3f),
			new Vector3(2f,0f)
		};

		
	

		
		List<Vector3> letterY = new List<Vector3> {
			new Vector3(2f,0f),
			new Vector3(2f,1f),
			new Vector3(2f,2f),
			new Vector3(2f,3f),
			new Vector3(2f,4f),
			new Vector3(1f,4f),
			new Vector3(3f,4f),
			new Vector3(0f,5f),
			new Vector3(4f,5f)
			
		};


		words.Add(letterG);
		words.Add(letterE);
		words.Add(letterR);
		words.Add(letterR);
		words.Add(letterY);
		
		StartCoroutine(drawWord(words));
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
