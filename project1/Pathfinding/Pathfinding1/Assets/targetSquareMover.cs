﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class targetSquareMover : MonoBehaviour {

	// Use this for initialization
	GameObject mainMap;


	void Start () {
		mainMap = GameObject.Find("MainMap");
		StartCoroutine(updateMap());
	}
	
	IEnumerator updateMap()
	{
		while(true)
		{
			mainMap.GetComponent<AstarPath>().Scan();
			yield return new WaitForSeconds(1f);
		}
	}




	// Update is called once per frame
	void Update () {

		transform.Translate(Vector3.right
		* Input.GetAxis("Horizontal")
		* Time.deltaTime * 10f);


		transform.Translate(Vector3.up
		* Input.GetAxis("Vertical")
		* Time.deltaTime * 10f);
	}
}
