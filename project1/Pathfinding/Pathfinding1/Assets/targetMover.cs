﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Pathfinding{
public class targetMover : MonoBehaviour {


	GameObject robot;

	// Use this for initialization
	void Start () {
		robot = GameObject.Find("AI");
		StartCoroutine(updateTarget());
	}

	IEnumerator updateTarget()
	{
		while (true)
		{
			//Debug.Log(Time.time);
			robot.GetComponent<AIDestinationSetter>().target = this.transform;
			robot.GetComponent<AILerp>().SearchPath();
			//AstarPath.active.Scan();
			yield return null;
		}
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(Vector3.up * Input.GetAxis("Vertical") * 15f * Time.deltaTime);
		transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * 15f * Time.deltaTime);
	}
}
}