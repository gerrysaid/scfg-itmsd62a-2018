﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerController : MonoBehaviour {


	Text raycastText;
	// Use this for initialization
	void Start () {
		raycastText = GameObject.Find("RaycastLabel").GetComponent<Text>();
	}
	
	// Update is called once per frame
	//we use FixedUpdate to wait for the physics engine to finalize all collisions before moving on to the next frame
	void FixedUpdate () {
		transform.Translate(Vector3.up * Input.GetAxis("Vertical") * Time.deltaTime * 10f);
		transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * Time.deltaTime * 10f);

		//raycasting here Vector3.right could be a shorthand for new Vector3(1f,0f)
		//this raycast is NOT range limited
		ContactFilter2D contactFilter = new ContactFilter2D();
		//only get obstacles in your raycast
		contactFilter.layerMask = LayerMask.GetMask("Obstacles");

		//limit of 10 objects hit 
		RaycastHit2D[] hits = new RaycastHit2D[10];

		//has a hit happened? the int gives me the number of objects intersecting the raycast
		int hit = Physics2D.Raycast(transform.position,new Vector3(1f,0f),contactFilter,hits);

		Debug.Log(hit);

		//if something has been hit, show me the first object intersecting the raycast
		if (hit>1){
			raycastText.text = "Name: "+hits[1].collider.gameObject.name
				+ "\nPosition: "+hits[1].transform.position
				+ "\nColour: "+hits[1].collider.gameObject.GetComponent<SpriteRenderer>().color;
		}

	}
}
