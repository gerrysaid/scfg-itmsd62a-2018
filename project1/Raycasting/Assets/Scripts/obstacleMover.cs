﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstacleMover : MonoBehaviour {


	Vector3 startPoint,endPoint;
	// Use this for initialization
	void Start () {
		startPoint = new Vector3(0.5f,2.7f);
		endPoint = new Vector3(0.5f,-2.7f);
	}
	
	// Update is called once per frame
	void Update () {

		//transform position for the obstacle
		transform.position = Vector3.Lerp(startPoint,endPoint,Mathf.PingPong(Time.time,1));
		


	}
}
