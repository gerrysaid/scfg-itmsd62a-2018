﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waypointController : MonoBehaviour {

	public List<Vector3> waypoints;

	GameObject cursor,myCursor,waypoint,myWaypoint;
	// Use this for initialization


	void Start () {
		waypoints = new List<Vector3>();

		cursor = Resources.Load<GameObject>("Prefabs/Circle");
		
		myCursor = Instantiate(cursor,new Vector3(0f,0f),Quaternion.identity);

		waypoint = Resources.Load<GameObject>("Prefabs/Waypoint");


	}
	
	// Update is called once per frame
	Vector3 myMousePosition;

	void Update () {

		//mouse screen coordinates in the console
		Debug.Log(Camera.main.ScreenToWorldPoint(Input.mousePosition));

		myMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		myCursor.transform.position = new Vector3(myMousePosition.x,
				myMousePosition.y);

		//left click
		if (Input.GetMouseButtonDown(0)){
			waypoints.Add(myCursor.transform.position);
			myWaypoint = Instantiate(waypoint,myCursor.transform.position,Quaternion.identity);
			//1,2,3,4 etc
			myWaypoint.GetComponentInChildren<TextMesh>().text = waypoints.Count.ToString();
		}

		
	}
}
