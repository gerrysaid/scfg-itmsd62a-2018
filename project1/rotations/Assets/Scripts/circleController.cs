﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class circleController : MonoBehaviour {

	public bool moveInSteps;

	GameObject objectIHit,lineTemplate,line;

	Material lineColour;
	// Use this for initialization

	void Start () {
		//NOTE the paths are CaSe SeNsitive
		lineTemplate = Resources.Load<GameObject>("Prefabs/LineObject");
		lineColour = Resources.Load<Material>("Materials/LineColour");

	}


	void OnTriggerEnter2D(Collider2D myCollisionObject)
	{
		objectIHit = myCollisionObject.gameObject;
		GetComponent<SpriteRenderer>().color = Color.red;
		myCollisionObject.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
		//create a new instance of line
		if (line == null){
			line = Instantiate(lineTemplate,transform.position,transform.rotation);
			//set the properties of the line
			line.AddComponent<LineRenderer>();
			//set the colour of the line
			line.GetComponent<LineRenderer>().sharedMaterial = lineColour;
			//set the start position of the line to the position of the collision
			line.GetComponent<LineRenderer>().SetPosition(0,objectIHit.transform.position);
			line.GetComponent<LineRenderer>().startWidth = 0.2f;
		} else {
			line.GetComponent<LineRenderer>().SetPosition(0,objectIHit.transform.position);
		}
	}

	void OnTriggerExit2D(Collider2D myCollisionObject)
	{
		GetComponent<SpriteRenderer>().color = Color.black;
		myCollisionObject.gameObject.GetComponent<SpriteRenderer>().color = Color.black;
	}
	
	// Update is called once per frame
	void Update () {
		//if a line has been created
		if (line !=null )
		{	
			//this.transform.position = the position of the circle, which is moving around
			line.GetComponent<LineRenderer>().SetPosition(1,this.transform.position);
			line.GetComponent<LineRenderer>().endWidth = 0.2f;
			//set the text mesh of the line to the distance between the box and the circle
			line.GetComponent<TextMesh>().text = 
				Vector3.Distance(this.transform.position,objectIHit.transform.position).ToString();

		}	


		if (!moveInSteps)
		{
			transform.Translate(Vector3.right * Input.GetAxis("Horizontal")*15f*Time.deltaTime);
			transform.Translate(Vector3.up * Input.GetAxis("Vertical")*15f*Time.deltaTime);
		}
		else {
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				transform.position += Vector3.up;
			}
			if (Input.GetKeyDown(KeyCode.DownArrow))
			{
				transform.position -= Vector3.up;
			}
			if (Input.GetKeyDown(KeyCode.LeftArrow))
			{
				transform.position -= Vector3.right;
			}
			if (Input.GetKeyDown(KeyCode.RightArrow))
			{
				transform.position += Vector3.right;
			}
		}
	}
}
