﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectGenerator : MonoBehaviour {

	GameObject boxtemplate,circletemplate,box1,box2,circle1,circle2;

	bool rotate1,rotate2;

	
	
	float rotationangle;
	// Use this for initialization
	void Start () {
		rotate1=rotate2=false;
		boxtemplate = Resources.Load<GameObject>("Prefabs/Square");
		circletemplate = Resources.Load<GameObject>("Prefabs/Circle");
		//--------------
		box1 = Instantiate(boxtemplate,new Vector3(-1f,1f),Quaternion.identity);
		box1.name = "Box1";
		box1.AddComponent<boxController>();
		
		box2 = Instantiate(boxtemplate,new Vector3(1f,1f),Quaternion.identity);
		box2.name = "Box2";
		box2.AddComponent<boxController>();
		

		circle1 = Instantiate(circletemplate,new Vector3(-1f,-1f),Quaternion.identity);
		circle1.name = "Circle1";
		circle1.AddComponent<circleController>();
		circle1.GetComponent<circleController>().moveInSteps = false;

		/*
		circle2 = Instantiate(circletemplate,new Vector3(1f,-1f),Quaternion.identity);
		circle2.name = "Circle2";
		circle2.AddComponent<circleController>();
		circle2.GetComponent<circleController>().moveInSteps = true;
		 */

		//--------------
		rotationangle = 45f;
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Q))
		{
			rotate1=!rotate1;
		}
		if (rotate1)
		{
			//rotate the box at a fixed speed
			box1.transform.Rotate(Vector3.forward*Time.deltaTime*20f);
		}

		if (Input.GetKeyDown(KeyCode.E))
		{
		
			//add 45 degrees to the rotation every time E is pressed
			box2.transform.rotation *= Quaternion.Euler(0f,0f,rotationangle);
			if (rotationangle < 360f)
				rotationangle += 45f;
			else 
				rotationangle = 0f;
			//every time E is pressed, add 45 to the angle you are adding and see what happens
		}



	}
}
