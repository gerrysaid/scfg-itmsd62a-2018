﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class dotsScript : MonoBehaviour
{

    // Use this for initialization
    Text timerText;

    List<Vector3> dotPositions;

    GameObject dot;

    Material lineMaterial;

    levelcontrollerscript levelscript;

    int score = 0;

    void Start()
    {
        timerText = GameObject.Find("timerText").GetComponent<Text>();
        levelscript = GameObject.Find("LevelController").GetComponent<levelcontrollerscript>();

        dot = Resources.Load<GameObject>("prefabs/Dot");
        lineMaterial = Resources.Load<Material>("black");
        dotPositions = new List<Vector3>();
        GenerateDots(levelscript.numberofdots);
    }


    void GenerateDots(int numberOfDots)
    {
        //dots must be on screen, so we must generate a random value in X on screen
        //we must also save all dots to a list, and if the dot is already in the list,
        //we must re generate a new dot.
        Vector3 topLeftCorner = new Vector3(
            -Camera.main.orthographicSize * Camera.main.aspect,
            Camera.main.orthographicSize
        );

        Vector3 topRightCorner = new Vector3(
            Camera.main.orthographicSize * Camera.main.aspect,
            Camera.main.orthographicSize
        );

        Vector3 bottomRightCorner = new Vector3(
            Camera.main.orthographicSize * Camera.main.aspect,
            -Camera.main.orthographicSize
        );

        Vector3 bottomLeftCorner = new Vector3(
            -Camera.main.orthographicSize * Camera.main.aspect,
            -Camera.main.orthographicSize
        );
        float xpos = 0f;
        float ypos = 0f;

        for (int i = 0; i < numberOfDots; i++)
        {
            xpos = UnityEngine.Random.Range(topLeftCorner.x + 0.5f, topRightCorner.x - 0.5f);
            ypos = UnityEngine.Random.Range(topLeftCorner.y - 0.5f, bottomLeftCorner.y + 0.5f);
            if (checkPositionInList(new Vector3(xpos, ypos)))
            {
                GameObject eachDot = Instantiate(dot, new Vector3(xpos, ypos), Quaternion.identity);
                eachDot.GetComponentInChildren<TextMesh>().text = i.ToString();
                dotPositions.Add(eachDot.transform.position);
            }
            else
            {
                i--;
            }
        }

    }


    //check if a given position exists in the list of positions
    bool checkPositionInList(Vector3 position)
    {
        foreach (Vector3 pos in dotPositions)
        {
            if (Vector3.Distance(position, pos) < 0.5f)
            {
                return false;
            }
        }
        return true;
    }

    int clickCount, startNumber, endNumber = 0;
    Vector3 startPoint, endPoint;

    IEnumerator restartGame()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("task3");
    }


    // Update is called once per frame
    void Update()
    {
        TimeSpan timeSpan = TimeSpan.FromSeconds(Time.time);
        string timeText = string.Format("{0:D2}:{1:D2}:{2:D2}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
        timerText.text = timeText;

        //every time mouse is clicked, cast a ray from under the mouse to check if it intersects with any other object. 
        //if another object is there, increase score and turn object red
        if (Input.GetMouseButtonDown(0))
        {

            //Debug.Log(clickCount);
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D objectHit = Physics2D.Raycast(mousePosition, Vector3.forward);

            if (objectHit.collider != null)
            {
                clickCount++;
                objectHit.collider.gameObject.GetComponent<SpriteRenderer>().color = Color.red;

                if (clickCount == 1)
                {
                    startPoint = objectHit.transform.position;
                    startNumber = int.Parse(objectHit.collider.gameObject.GetComponentInChildren<TextMesh>().text);
                    Debug.Log("S:" + startNumber);

                }
                if (clickCount == 2)
                {
                    endPoint = objectHit.transform.position;
                    endNumber = int.Parse(objectHit.collider.gameObject.GetComponentInChildren<TextMesh>().text);
                    Debug.Log("E:" + endNumber);

                    if (startNumber + 1 == endNumber)
                    {
                        //can draw line
                        GameObject line = new GameObject();
                        line.AddComponent<LineRenderer>();
                        line.GetComponent<LineRenderer>().material = lineMaterial;
                        line.GetComponent<LineRenderer>().startWidth = 0.2f;
                        line.GetComponent<LineRenderer>().endWidth = 0.2f;
                        line.GetComponent<LineRenderer>().startColor = Color.black;
                        line.GetComponent<LineRenderer>().endColor = Color.black;
                        Vector3[] linePositions = new Vector3[2];
                        linePositions[0] = startPoint;
                        linePositions[1] = endPoint;
                        line.GetComponent<LineRenderer>().SetPositions(linePositions);
                        startNumber = endNumber;
                        startPoint = endPoint;
                        clickCount = 1;
                        //when are we at the last point, reset the scene at that point?
                        if (endNumber == (dotPositions.Count - 1))
                        {
                            Debug.Log("You win!");
                            levelscript.numberofdots++;
                            StartCoroutine(restartGame());
                        }


                    }
                    else
                    {
                        Debug.Log("Connect the dots in order!");
                        StartCoroutine(restartGame());

                    }

                }


            }

        }



    }
}
