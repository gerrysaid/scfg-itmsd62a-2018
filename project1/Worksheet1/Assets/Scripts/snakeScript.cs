﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class snakeScript : MonoBehaviour
{

    GameObject snakePrefab, snakehead, foodPrefab, food;

    Vector3 bottomLeft;

    public List<Vector3> trail;

	public List<GameObject> foods;

    int foodAmount, foodcounter, foodeaten;

    // Use this for initialization
    void Start()
    {
        foodAmount = 30;
        foodeaten = 0;
        snakePrefab = Resources.Load<GameObject>("prefabs/SnakeSquare");
        foodPrefab = Resources.Load<GameObject>("prefabs/FoodSquare");
        trail = new List<Vector3>();
        foods = new List<GameObject>();
        bottomLeft = Camera.main.ScreenToWorldPoint(new Vector3(0f, 0f));
        snakehead = Instantiate(snakePrefab, Random.insideUnitCircle * 3, Quaternion.identity);
        snakehead.name = "Head";
        snakehead.GetComponent<SpriteRenderer>().color = Color.gray;
        StartCoroutine(spawnFood());
    }


    void drawTrail(int distance)
    {
        //clear the previous trail
        foreach (GameObject snakebox in GameObject.FindGameObjectsWithTag("snake"))
        {
            if (snakebox.name != "Head")
                Destroy(snakebox);
        }

        //for each element in trail, starting from the last, to distance, draw the trail

        for (int i = distance; i > 0; i--)
        {
            if (distance < trail.Count - 1)
            {
                Instantiate(snakePrefab, trail[(trail.Count - 1) - i], Quaternion.identity);
            }
        }
    }

    bool checkCollision(int distance)
    {
        for (int i = distance; i > 0; i--)
        {
            if (distance < trail.Count - 1)
            {
                if (Vector3.Distance(snakehead.transform.position, trail[(trail.Count - 1) - i]) < 0.5f)
                {
                    return true;
                }
            }
        }
        return false;
    }



    bool checkCollisionWithFood()
    {
        foreach (GameObject food in foods)
        {

            if (Vector3.Distance(snakehead.transform.position, food.transform.position) < 0.5f)
            {
				//Debug.Log(Vector3.Distance(snakehead.transform.position, food.transform.position));
				foods.Remove(food);
				Destroy(food);
                return true;
            }

        }
        return false;
    }

    bool checkPositionInList(Vector3 position)
    {
        foreach (GameObject pos in foods)
        {
            if (Vector3.Distance(position, pos.transform.position) < 0.5f)
            {
                return false;
            }
        }
        return true;
    }

    IEnumerator spawnFood()
    {
        while (true)
        {
            if (foodcounter < foodAmount)
            {
                //Instantiate(foodPrefab,Random.insideUnitCircle*3,Quaternion.identity);
                //use the dot spawning method so food is not spawned in the same place
				Vector3 topLeftCorner = new Vector3(
					-Camera.main.orthographicSize * Camera.main.aspect,
					Camera.main.orthographicSize
				);

                Vector3 topRightCorner = new Vector3(
                    Camera.main.orthographicSize * Camera.main.aspect,
                    Camera.main.orthographicSize
                );

                Vector3 bottomRightCorner = new Vector3(
                    Camera.main.orthographicSize * Camera.main.aspect,
                    -Camera.main.orthographicSize
                );

                Vector3 bottomLeftCorner = new Vector3(
                    -Camera.main.orthographicSize * Camera.main.aspect,
                    -Camera.main.orthographicSize
                );
                float xpos = 0f;
                float ypos = 0f;

                for (int i = 0; i < foodAmount; i++)
                {
                    xpos = UnityEngine.Random.Range(topLeftCorner.x + 0.5f, topRightCorner.x - 0.5f);
                    ypos = UnityEngine.Random.Range(topLeftCorner.y - 0.5f, bottomLeftCorner.y + 0.5f);
                    xpos = Mathf.Round(xpos*2f)/2f;
                    ypos = Mathf.Round(ypos*2f)/2f;


                    if (checkPositionInList(new Vector3(xpos, ypos)))
                    {
                        GameObject eachFood = Instantiate(foodPrefab, new Vector3(xpos, ypos), Quaternion.identity);

                        foods.Add(eachFood);
                        foodcounter++;
                        yield return new WaitForSeconds(Random.Range(2f,5f));   
                    }
                    else
                    {
                        i--;
                    }
                }



            }
            yield return null;
        }
    }


    void updateTrail()
    {
        if (checkCollision(foodeaten))
        {
            //snake has hit itself
            //reset scene/reset food amount
            Debug.Log("Snake hits itself");
            SceneManager.LoadScene("task4");
        }
        if (checkCollisionWithFood())
        {
            //snake has hit itself
            //reset scene/reset food amount
            Debug.Log("Snake hits food");
            foodeaten++;
        }

        trail.Add(snakehead.transform.position);
        drawTrail(foodeaten);

    }


    void moveSnake()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            snakehead.transform.position += new Vector3(0f, 1f);
            updateTrail();

        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            snakehead.transform.position -= new Vector3(0f, 1f);
            updateTrail();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            snakehead.transform.position -= new Vector3(1f, 0f);
            updateTrail();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            snakehead.transform.position += new Vector3(1f, 0f);
            updateTrail();
        }


    }

    // Update is called once per frame
    void Update()
    {
        moveSnake();

    }
}
